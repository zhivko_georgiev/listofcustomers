package com.sample.ListOfCustomersDesktopApp.Models;

import java.nio.file.Path;
import java.nio.file.Paths;

import javafx.beans.property.SimpleStringProperty;

public class Customer {
	private SimpleStringProperty name;
	private SimpleStringProperty town;
	private SimpleStringProperty contractSignDate;
	private SimpleStringProperty notes;
	private SimpleStringProperty contract;
	private SimpleStringProperty logo;

	public Customer(String name, String... params) {
		this.name = new SimpleStringProperty(name);

		String town = params.length > 0 ? params[0] : null;
		String contractSignDate = params.length > 1 ? params[1] : null;
		String notes = params.length > 2 ? params[2] : null;
		String contract = params.length > 3 ? params[3] : null;
		String logo = params.length > 4 ? params[4] : null;

		if (town != null) {
			this.town = new SimpleStringProperty(town);
		}

		if (contractSignDate != null) {
			this.contractSignDate = new SimpleStringProperty(contractSignDate);
		}

		if (notes != null) {
			this.notes = new SimpleStringProperty(notes);
		}

		if (contract != null) {
			this.contract = new SimpleStringProperty(contract);
		}

		if (logo != null) {
			this.logo = new SimpleStringProperty(logo);
		}
	}

	public String getName() {
		return this.name.get();
	}

	public void setName(String name) {
		this.name.set(name);
	}

	public String getTown() {
		return this.town.get();
	}

	public void setTown(String town) {
		this.town.set(town);
	}

	public String getContractSignDate() {
		return this.contractSignDate.get();
	}

	public void setContractSignDate(String contractSignDate) {
		this.contractSignDate.set(contractSignDate);
	}

	public String getNotes() {
		return this.notes.get();
	}

	public void setNotes(String notes) {
		this.notes.set(notes);
	}

	public String getContract() {
		return this.contract.get();
	}

	public void setContract(String contract) {
		this.contract.set(contract);
	}
	
	public String getContractName() {
		Path p = Paths.get(this.contract.get());
		String contractName = p.getFileName().toString();
		
		return contractName;
	}

	public String getLogo() {
		return this.logo.get();
	}

	public void setLogo(String logo) {
		this.logo.set(logo);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(this.getName() + "\n");

		if (this.getTown() != null) {
			sb.append(this.getTown() + "\n");
		}

		if (this.getContractSignDate() != null) {
			sb.append(this.getContractSignDate() + "\n");
		}

		if (this.getNotes() != null) {
			sb.append(this.getNotes() + "\n");
		}

		if (this.getContract() != null) {
			sb.append(this.getContract() + "\n");
		}

		if (this.getLogo() != null) {
			sb.append(this.getLogo() + "\n");
		}

		return sb.toString();
	}
}
