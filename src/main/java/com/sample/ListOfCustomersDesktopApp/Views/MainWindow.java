package com.sample.ListOfCustomersDesktopApp.Views;

import java.util.Map;
import java.util.Map.Entry;

import com.sample.ListOfCustomersDesktopApp.Models.Customer;
import com.sample.ListOfCustomersDesktopApp.Utils.JDOMParser;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;

public class MainWindow extends Application {

    public static void main(String[] args) {
        launch(args);
    }
    
	@Override
	public void start(Stage stage) {
		Map<String, Customer> listOfCustomers = JDOMParser.parsedInput();

		// use fully detailed type for Map.Entry<String, String>
		TableColumn<Map.Entry<String, Customer>, String> column1 = new TableColumn<>(
				"Име");
		column1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map.Entry<String, Customer>, String>, ObservableValue<String>>() {

			@Override
			public ObservableValue<String> call(
					TableColumn.CellDataFeatures<Map.Entry<String, Customer>, String> p) {
				// this callback returns property for just one cell, you can't
				// use a loop here
				// for first column we use key
				return new SimpleStringProperty(p.getValue().getKey());
			}
		});

		TableColumn<Map.Entry<String, Customer>, String> column2 = new TableColumn<>(
				"Населено Място");
		column2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map.Entry<String, Customer>, String>, ObservableValue<String>>() {

			@Override
			public ObservableValue<String> call(
					TableColumn.CellDataFeatures<Map.Entry<String, Customer>, String> p) {
				// for second column we use value
				return new SimpleStringProperty(p.getValue().getValue().getTown());
			}
		});
		
		TableColumn<Map.Entry<String, Customer>, String> column3 = new TableColumn<>(
				"Дата на подписване на договор");
		column3.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map.Entry<String, Customer>, String>, ObservableValue<String>>() {

			@Override
			public ObservableValue<String> call(
					TableColumn.CellDataFeatures<Map.Entry<String, Customer>, String> p) {
				// for second column we use value
				return new SimpleStringProperty(p.getValue().getValue().getContractSignDate());
			}
		});
		
		TableColumn<Map.Entry<String, Customer>, String> column4 = new TableColumn<>(
				"Бележки");
		column4.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map.Entry<String, Customer>, String>, ObservableValue<String>>() {

			@Override
			public ObservableValue<String> call(
					TableColumn.CellDataFeatures<Map.Entry<String, Customer>, String> p) {
				// for second column we use value
				return new SimpleStringProperty(p.getValue().getValue().getNotes());
			}
		});
		
		TableColumn<Map.Entry<String, Customer>, String> column5 = new TableColumn<>(
				"Договор");
		column5.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map.Entry<String, Customer>, String>, ObservableValue<String>>() {

			@Override
			public ObservableValue<String> call(
					TableColumn.CellDataFeatures<Map.Entry<String, Customer>, String> p) {
				// for second column we use value
				return new SimpleStringProperty(p.getValue().getValue().getContractName());
			}
		});
		
		ObservableList<Entry<String, Customer>> items = FXCollections
				.observableArrayList(listOfCustomers.entrySet());
		final TableView<Entry<String, Customer>> table = new TableView<>(
				items);

		table.getColumns().setAll(column1, column2, column3, column4, column5);

        final Button addButton = new Button("Добави");
        final Button editButton = new Button("Редактирай");
        final Button deleteButton = new Button("Изтрий");

        final HBox hb = new HBox();
        
        hb.getChildren().addAll(addButton, editButton, deleteButton);
        hb.setSpacing(3);
        
        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(table, hb);
		
		Scene scene = new Scene(vbox, 800, 600);
		stage.setScene(scene);
		stage.show();
	}
}