package com.sample.ListOfCustomersDesktopApp.Utils;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

public class SomeStuffs {

	public static void main(String[] args) {

		// Open the file with the default program
		try {
			File file = new File(
					"P:\\List Of Clients Desktop App.jpeg");

			if (file.exists() && !file.isDirectory()) {
				Desktop.getDesktop().open(file);
			} else {
				System.out.println("Such File Doesn't Exist!");
			}

		} catch (IOException e) {
			// TODO: handle exception
		} 
	}
}
