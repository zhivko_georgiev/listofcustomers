package com.sample.ListOfCustomersDesktopApp.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.sample.ListOfCustomersDesktopApp.Models.Customer;


public class JDOMParser {

	public static Map<String, Customer> parsedInput() {
		Map<String, Customer> listOfCustomers = new HashMap<>();

		try {
			InputStream inputStream = JDOMParser.class.getResourceAsStream("/data.xml");
			
			SAXBuilder saxBuilder = new SAXBuilder();

			Document document = saxBuilder.build(inputStream);

			Element classElement = document.getRootElement();

			List<Element> customersList = classElement.getChildren();

			for (int current = 0; current < customersList.size(); current++) {
				Element customer = customersList.get(current);

				String name = customer.getChild("name").getText();
				String town = customer.getChild("town").getText();
				String contractSignDate = customer.getChild("contractSignDate")
						.getText();
				String notes = customer.getChild("notes").getText();
				String contract = customer.getChild("contract").getText();
				String logo = customer.getChild("logo").getText();

				Customer customerToBeAdded = new Customer(name, town,
						contractSignDate, notes, contract, logo);

				if (!listOfCustomers.containsKey(name)) {
					listOfCustomers.put(name, customerToBeAdded);
				}
			}
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return listOfCustomers;
	}
}